package airthmetic.linked;

import lombok.Data;

@Data
class Node{
    private Node next;
    private int data;
    public Node(int data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "data=" + data + "-->"+next;
    }
}