package proxy.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * JDK动态代理
 *
 * @author zhibai
 */
public class DynamicProxy implements InvocationHandler {

    private Object object;

    public DynamicProxy(Object object) {
        this.object = object;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("动态代理---访问真实主题之前的预处理。");
        Object result = method.invoke(object, args);
        System.out.println("动态代理---访问真实主题之后的后续处理。");
        return result;
    }
}
