package com.zhuawa.course.biz.redis;

import com.zhuawa.course.persistence.user.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisConfigTest {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private RedisTemplate redisTemplate;

    @Resource
    private ValueOperations<String,Object> valueOperations;

    @Autowired
    private HashOperations<String, String, Object> hashOperations;

    @Autowired
    private ListOperations<String, Object> listOperations;

    @Autowired
    private SetOperations<String, Object> setOperations;

    @Autowired
    private ZSetOperations<String, Object> zSetOperations;

    @Resource
    private RedisService redisService;

    @Test
    public void testObj() throws Exception{
        User user = new User();
        user.setName("测试dfas");
        user.setAge(123);
        ValueOperations<String,Object> operations = redisTemplate.opsForValue();
        redisService.expireKey("name",20, TimeUnit.SECONDS);
    }

    @Test
    public void testValueOption( )throws  Exception{
        User user = new User();
        user.setName("jantent");
        user.setAge(23);
        valueOperations.set("test",user);

        System.out.println(valueOperations.get("test"));
    }

    @Test
    public void testSetOperation() throws Exception{
        User user = new User();
        user.setName("jantent");
        user.setAge(23);
        User auser = new User();
        auser.setName("antent");
        auser.setAge(23);
        setOperations.add("User:test",user,auser);
        Set<Object> result = setOperations.members("User:test");
        System.out.println(result);
    }

    @Test
    public void HashOperations() throws Exception{
        User user = new User();
        user.setName("jantent");
        user.setAge(23);
        hashOperations.put("hash:User",user.hashCode()+"",user);
        System.out.println(hashOperations.get("hash:User",user.hashCode()+""));
    }

    @Test
    public void  ListOperations() throws Exception{
        User user = new User();
        user.setName("jantent");
        user.setAge(23);
//        listOperations.leftPush("list:User",user);
//        System.out.println(listOperations.leftPop("list:User"));
        // pop之后 值会消失
        System.out.println(listOperations.leftPop("list:User"));
    }
}